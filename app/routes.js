var Medication = require('./models/medication');
var User = require('./models/user');
var crypto = require('crypto');

module.exports = function(app) {

	app.post('/api/users', function(req, res) {

		req.body.password = crypto.createHash('md5').update(req.body.password).digest('hex');

		var user = new User();
		user.username = req.body.username;
		user.password = req.body.password;

		user.save(function(err) {

			if (err) {

				res.send(err);

			};

			res.json({message: 'User created!'});

		});

	});

	app.post('/api/users/:username', function(req, res) {

		req.body.password = crypto.createHash('md5').update(req.body.password).digest('hex');

		User.find({'username': req.params.username}, function(err, user) {

			user = user[0];
			if (err) {

				res.json({message: 'Invalid username!'});

			} else if (user.password === req.body.password) {

				res.json({message: 'Successfully logged in!'})

			} else {

				res.json({message: 'Invalid password!'})

			};

		});

	});

	app.post('/api/medications', function(req, res) {

		var medication = new Medication();
		medication.name = req.body.name;
		medication.indications = req.body.indications;
		medication.sideEffects = req.body.sideEffects;

		medication.save(function(err) {

			if (err) {

				res.send(err);

			};

			res.json({message: 'Medication created!'});

		});

	});

	app.get('/api/medications/:medication_id', function(req, res) {

		Medication.findById(req.params.medication_id, function(err, medication) {

			if (err) {

				res.send(err);

			};

			res.json(medication);

		});

	});

	app.put('/api/medications/:medication_id', function(req, res) {

		Medication.findById(req.params.medication_id, function(err, medication) {

			if (err) {

				res.send(err);

			};

			medication.name = req.body.name;
			medication.indications = req.body.indications;
			medication.sideEffects = req.body.sideEffects;

			medication.save(function(err) {

				if(err) {

					res.send(err);

				};

				res.json({message: 'Medication updated!'})

			});

		});

	});

	app.delete('/api/medications/:medication_id', function(req, res) {

		Medication.remove({

			_id: req.params.medication_id

		}, function(err, bear) {

			if (err) {

				res.send(err);

			};

			res.json({message: 'Medication deleted!'})

		})

	});

	app.get('/api/medications', function(req, res) {

		Medication.find(function(err, medications) {

			if (err) {

				res.send(err);

			};

			res.json(medications);

		});

	});

	app.get('*', function(req, res) {

		res.sendfile('./public/views/index.html');

	});

};