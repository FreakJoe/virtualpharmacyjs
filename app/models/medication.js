var mongoose = require('mongoose');
var db = require('../../config/db');
mongoose.connect(db.url);

module.exports = mongoose.model('Medication', {

	name: {type: String, default: ''},
	indications: {type: String, default: ''},
	sideEffects: {type: String, default: ''}
	
});