angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/create', {
			templateUrl: 'views/create.html',
			controller: 'MainController'
		})

		.when('/register', {
			templateUrl: 'views/register.html',
			controller: 'MainController'
		})

		.when('/login', {
			templateUrl: 'views/login.html',
			controller: 'MainController'
		})

		.when('/edit', {
			templateUrl: 'views/edit.html',
			controller: 'MainController'
		});

	$locationProvider.html5Mode(true);

}]).run(run);

function run($rootScope, $location, UserService) {

	$rootScope.$on('$locationChangeStart', function(event, next, current) {

		var restrictedPage = contains(['/create', '/edit'], $location.path());
		var loggedIn = UserService.session().loggedIn === 'true';
		if (restrictedPage && !loggedIn) {

			$location.path('/login');

		};

	});

};

function contains(a, obj) {

	var i = a.length;
	while (i--) {

		if (a[i] === obj) {

			return true;

		}

	}
	return false;

};