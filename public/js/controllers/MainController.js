angular.module('MainCtrl', ['MedicationService', 'UserService']).controller('MainController', function($http, $scope, MedicationService, UserService, $q, $window) {

	MedicationService.get().success(function(data) {

		$scope.medications = data;

	});

	$scope.medication = {};

	$scope.create = function() {

		MedicationService.create({
			name: this.name,
			indications: this.indications,
			sideEffects: this.sideEffects
		});

		this.name = '';
		this.indications = '';
		this.sideEffects = '';

	};

	$scope.register = function() {

		UserService.create({
			username: this.username,
			password: this.password
		});

		this.username = '';
		this.password = '';

	};

	$scope.login = function() {

		UserService.login({
			username: this.username,
			password: this.password
		});

		this.username = '';
		this.password = '';

	};

	$scope.logout = function() {

		UserService.logout();

	};

	$scope.notLoggedIn = function() {

		return UserService.session().loggedIn === 'false';

	};

	$scope.loggedIn = function() {

		return UserService.session().loggedIn === 'true';

	};

	$scope.setMedication = function(medication) {

		MedicationService.setCurrent(medication);

	};

	$scope.getMedication = function() {

		$scope.medication = MedicationService.getCurrent();
		if (!$scope.medication) {

			$window.location.href = '/';

		}

	};

	$scope.saveMedication = function() {

		MedicationService.save($scope.medication);

	};

})