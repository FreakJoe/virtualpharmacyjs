angular.module('UserService', ['ngCookies']).factory('UserService', ['$http', '$q', '$cookies', '$window', function($http, $q, $cookies, $window) {

	return {

		create: function(userData) {

			$window.location.href = '/';
			return $http.post('/api/users/', userData);

		},

		login: function(userData) {

			return $http.post('/api/users/' + userData['username'], userData).success(function(data) {

				var success = (data.message === 'Successfully logged in!');
				if (success) {
					$cookies.put('loggedIn', true);
					$cookies.put('username', userData['username']);
					$window.location.href = '/';
				}

			});

		},

		logout: function() {

			$cookies.put('loggedIn', false);
			$cookies.put('username', '');

		},

		session: function() {

			return $cookies.getAll();

		}

	};

}]);