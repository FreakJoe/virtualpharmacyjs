angular.module('MedicationService', []).factory('MedicationService', ['$http', '$q', function($http, $q) {

	var curMedication = "";

	return {

		get: function() {

			return $http.get('/api/medications');

		},

		create: function(medicationData) {

			return $http.post('/api/medications', medicationData);

		},

		delete: function(id) {

			return $http.delete('/api/medications/' + id);

		},

		save: function(medication) {

			return $http.put('/api/medications/' + medication._id, medication);

		},

		setCurrent: function(medication) {

			curMedication = medication;

		},

		getCurrent: function() {

			return curMedication;

		}

	};

}]);